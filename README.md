# Visão Geral do Projeto 

## Planejamento 

Neste [documento](READMEDSM.md) há sugestão de execução do projeto. 

## Epics do Projeto 

| EPIC                                                                                          | Tipo           | EPIC-ID   | Link                     |
|:----------------------------------------------------------------------------------------------|:---------------|:----------|:-------------------------|
| Eu, como Desenvolvedor, quero mapear OntoUML para o Modelo de Informação                      | Model Mapping  | MM-01     | [link](./epics/MM-01.md) |
| Eu, como Desenvolvedor, quero adicionar elementos na gramatica para Arquitetura de WebService | Design Grammar | GR-01     | [link](./epics/GR-01.md) |
| Eu, como Desenvolvedor, quero mapear o modelo de Informação para Arquitetura de Webservice    | Design Mapping | DM-01     | [link](./epics/DM-01.md) |
| Eu, como Desenvoledor, quero criar documentação de design do código gerado                    | Documentation  | DO-01     | [link](./epics/DO-01.md) |
| Eu, como Aluno, quero escrever o meu TCC                                                      | TCC            | TCC-1     | [link](./epics/TCC-1.md) |