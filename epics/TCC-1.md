# [TCC-1] - Eu, como Aluno, quero escrever o meu TCC 
|    | US                                                             | US-ID   |
|---:|:---------------------------------------------------------------|:--------|
|  4 | Eu, como Aluno, quero escrever a introdução o meu TCC          | TCC-1-0 |
|  5 | Eu, como Aluno, quero escrever o referencial teorico o meu TCC | TCC-1-1 |
|  6 | Eu, como Aluno, quero escrever a implementação o meu TCC       | TCC-1-2 |
|  7 | Eu, como Aluno, quero escrever a conclusão o meu TCC           | TCC-1-3 |
|  8 | Eu, como Aluno, quero definir a data da apresentação o meu TCC | TCC-1-4 |

## Relação entre as histórias de usuário 

![graph](TCC-1.png) 



## [TCC-1-0] - Eu, como Aluno, quero escrever a introdução o meu TCC 


| TASK                  | TASK-ID   |
|:----------------------|:----------|
| Escrever a introdução | TCC-1-0-0 |
| Escrever os objetivos | TCC-1-0-1 |
| Escrever Organização  | TCC-1-0-2 |

## [TCC-1-1] - Eu, como Aluno, quero escrever o referencial teorico o meu TCC 


| TASK                      | TASK-ID   |
|:--------------------------|:----------|
| Escrever introdução do BG | TCC-1-1-0 |
| Escrever conclusão do BG  | TCC-1-1-1 |

## [TCC-1-2] - Eu, como Aluno, quero escrever a implementação o meu TCC 


| TASK                       | TASK-ID   |
|:---------------------------|:----------|
| Escrever introdução do DES | TCC-1-2-0 |
| Escrever conclusão do DES  | TCC-1-2-1 |

## [TCC-1-3] - Eu, como Aluno, quero escrever a conclusão o meu TCC 


| TASK                             | TASK-ID   |
|:---------------------------------|:----------|
| Escrever introdução do Conclusão | TCC-1-3-0 |
| Escrever conclusão do conclusão  | TCC-1-3-1 |
| Escrever trabalhos futuros       | TCC-1-3-2 |

## [TCC-1-4] - Eu, como Aluno, quero definir a data da apresentação o meu TCC 


| TASK                   | TASK-ID   |
|:-----------------------|:----------|
| Fazer a apresentação   | TCC-1-4-0 |
| Validar a apresentação | TCC-1-4-1 |

