
Analytics
=========

# Relatório Analitico do DSM

- É um Grafo Direcionado? &rarr; True
- Há Ciclos no Grafo? &rarr; False

## Analisando o DSM

## Relação de Dependência e Consequencia entre os elementos
  

|Elemento|Qtd Dependencias|Dependencias|Qtd Habilitados|Habilitados|
| :---: | :---: | :---: | :---: | :---: |
|MM-01|[]|0|4|['GR-01', 'DM-01', 'DO-01', 'TCC-1']|
|GR-01|['MM-01']|1|3|['DM-01', 'DO-01', 'TCC-1']|
|DM-01|['MM-01', 'GR-01']|2|2|['DO-01', 'TCC-1']|
|DO-01|['MM-01', 'GR-01', 'DM-01']|3|1|['TCC-1']|
|TCC-1|['MM-01', 'GR-01', 'DM-01', 'DO-01']|4|0|[]|

## Sugestão de Ordem de execução

1. MM-01: Eu, como Desenvolvedor, quero mapear OntoUML para o Modelo de Informação
2. GR-01: Eu, como Desenvolvedor, quero adicionar elementos na gramatica para Arquitetura de WebService
3. DM-01: Eu, como Desenvolvedor, quero mapear o modelo de Informação para Arquitetura de Webservice
4. DO-01: Eu, como Desenvoledor, quero criar documentação de design do código gerado
5. TCC-1: Eu, como Aluno, quero escrever o meu TCC

## Elementos fora do grafo



![project](./epics/graph-project.png)